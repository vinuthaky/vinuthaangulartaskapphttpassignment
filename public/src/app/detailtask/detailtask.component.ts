import { Component, OnInit } from '@angular/core';
import { Task } from '../task';
import { TasksService } from '../tasks.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-detailtask',
  templateUrl: './detailtask.component.html',
  styleUrls: ['./detailtask.component.css']
})

export class DetailtaskComponent implements OnInit {
  private task: Task;
  constructor( private tasks: TasksService,
               private router: Router,
               private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      const  index = parseInt(params.get('id'), 10);
      this.tasks.getTaskById(index).subscribe(task => this.task = task);
    });
  }

}
