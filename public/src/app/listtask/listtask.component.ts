import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TasksService } from '../tasks.service';
import { Task } from '../task';

@Component({
  selector: 'app-listtask',
  templateUrl: './listtask.component.html',
  styleUrls: ['./listtask.component.css']
})

export class ListtaskComponent implements OnInit {
  private task: Task[] = [];
  constructor(  private _service: TasksService,
                private router: Router) { }

  ngOnInit() {
    this._service.getTasks().subscribe(data => {
      this.task = data;
      this.task.sort(function (a, b) {
                  return a.id - b.id;
      });
    });
  }

  delete(id, i) {
    this._service.deleteTask(id).subscribe();
    this.task.splice(i, 1);
  }

  edit(task) {
    this.router.navigate(['/edit', task.id]);
  }

  showDetails(task) {
    this.router.navigate(['/detail', task.id]);
  }

}
