import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TasksService } from '../tasks.service';

@Component({
  selector: 'app-addtask',
  templateUrl: './addtask.component.html',
  styleUrls: ['./addtask.component.css']
})
export class AddtaskComponent  {

  constructor( private tasks: TasksService,
               private router: Router) { }

  onClickSubmit(data) {
    this.tasks.addTask(data).subscribe(() => {
      this.router.navigate(['/list']);
    });
  }

}
